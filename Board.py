import sys

import chess
import chess.svg

from PyQt5 import QtGui
from PyQt5.QtCore import pyqtSlot, Qt
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QGridLayout, QLabel, QVBoxLayout, QPushButton

from EventMessage import EventMessage
from Cell import Cell
from CellNotation import CellNotation
from Constants import Constants
from BasicCell import *
from PiecePickerWidget import PiecePicker


class Board(QWidget):
	actionSignal = pyqtSignal('PyQt_PyObject')
	resultScreenSignal = pyqtSignal(str)
	def __init__(self, parent):
		super().__init__(parent)

		self.resize(Constants.BOARD_WIDTH, Constants.BOARD_HEIGHT)
		self.setStyleSheet('''background: #ccc''')
		self.chessboard = chess.Board()
		self.piecesDict = self.chessboard.piece_map()
		self.squareLabels = []

		self.row = 7
		self.col = 0



		self.selectedPiece = None
		self.targetSquare = None
		self.pickedPieceType = None
		self.startNewGame()

	def startNewGame(self):
		# self.clearSquareSelection()
		self.squareLabels.clear()
		self.row = 7
		self.col = 0
		self.chessboard.reset()
		self.createCells()

	def mirrorBoard(self):
		pass

	def createCells(self):
		for index, square in enumerate(chess.SQUARES):
			cell = BoardCell(self, square)
			cell.show()

			if index % 8 == 0:
				if index != 0:
					self.row -= 1
					self.col = 0
			else:
				self.col += 1

			if self.col % 2 == 0:
				if self.row % 2 == 0:
					cell.setBgColor('white')
				else:
					cell.setBgColor('#5c5c5c')
			else:
				if self.row % 2 == 0:
					cell.setBgColor('#5c5c5c')
				else:
					cell.setBgColor('white')

			cell.move(Constants.CELL_SIZE * self.col + Constants.OFFSET_BOARD_X, self.row * Constants.CELL_SIZE + Constants.OFFSET_BOARD_Y)
			self.squareLabels.append(cell)

			cell.mySignal.connect(self.handleClickSquare)
		self.renderPieces()
		self.createNotation()

	def createNotation(self):
		startX = Constants.OFFSET_BOARD_X
		startY = Constants.CELL_SIZE * 8 + Constants.OFFSET_BOARD_Y
		for fileIndex, file in enumerate(chess.FILE_NAMES):
			cellChar = BasicCell(self)
			cellChar.setText(file)
			cellChar.setAlignment(Qt.AlignCenter)
			cellChar.setBgColor('transparent')
			cellChar.move(startX + Constants.CELL_SIZE * fileIndex, startY)

		sX = startX - Constants.CELL_SIZE
		sY = startY - Constants.CELL_SIZE

		for rankIndex, rank in enumerate(chess.RANK_NAMES):
			cellChar = BasicCell(self)
			cellChar.setText(rank)
			cellChar.setBgColor('transparent')
			cellChar.setAlignment(Qt.AlignCenter)
			cellChar.move(sX, sY - Constants.CELL_SIZE * rankIndex)

	def renderPieces(self):
		for square in self.squareLabels:
			if square.squareIndex in self.piecesDict:
				square.setPiece(self.piecesDict[square.squareIndex])



	def updatePieces(self):
		for square in self.squareLabels:
			piece = self.chessboard.piece_at(square.squareIndex)
			square.setPiece(piece)

	def showPicker(self, color):
		self.piecePicker = PiecePicker(self, color)
		self.piecePicker.show()
		self.piecePicker.pickerSignal.connect(self.handlePicker)
		self.piecePicker.raise_()

	def hidePicker(self):
		self.piecePicker.close()

	def clearSquareSelection(self):
		if self.selectedPiece is not None:
			self.selectedPiece.activeSquare = False
			self.selectedPiece.changeActiveSelection()
		self.selectedPiece = None
		self.targetSquare = None

	def moveSelectedPieceTo(self, squareIndex, promotion=None):
		move = chess.Move(self.selectedPiece.squareIndex, squareIndex)
		if promotion is not None:
			move.promotion = promotion
		if move in self.chessboard.legal_moves:
			self.chessboard.push(move)

			self.clearSquareSelection()
			self.updatePieces()

			if self.chessboard.is_game_over():
				self.resultScreenSignal.emit('game_over')
				print(self.chessboard.result())

		else:
			# print('illegal move')
			self.clearSquareSelection()






	@pyqtSlot()
	def handleClickSquare(self):
		clickedSquare = self.sender()
		# m = EventMessage('click_action', clickedSquare.squareIndex)
		# self.actionSignal.emit(m)

		if self.selectedPiece is None:
			if self.chessboard.piece_at(clickedSquare.squareIndex):
				self.selectedPiece = clickedSquare
				self.selectedPiece.activeSquare = True
				self.selectedPiece.changeActiveSelection()
				return
		else:
			if self.selectedPiece.squareIndex == clickedSquare.squareIndex:
				self.clearSquareSelection()
				return
			# move logic
			if self.isLastRow(clickedSquare.squareIndex):
				self.showPicker(self.chessboard.turn)
				self.targetSquare = clickedSquare.squareIndex
				return
			else:
				self.moveSelectedPieceTo(clickedSquare.squareIndex)
			# // move logic

	def isLastRow(self, squareIndex):
		currentTurn = self.chessboard.turn
		if currentTurn:
			return chess.square_rank(squareIndex) == 7 and self.chessboard.piece_type_at(self.selectedPiece.squareIndex) == chess.PAWN
		else:
			return chess.square_rank(squareIndex) == 0 and self.chessboard.piece_type_at(self.selectedPiece.squareIndex) == chess.PAWN

	@pyqtSlot()
	def handlePicker(self):
		clickedItem = self.sender()

		self.moveSelectedPieceTo(self.targetSquare, clickedItem.pickedPiece.piece.piece_type)
		self.hidePicker()

