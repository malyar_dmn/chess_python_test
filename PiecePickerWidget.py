import sys

import chess
import chess.svg

from PyQt5 import QtGui
from PyQt5.QtCore import pyqtSlot, Qt, pyqtSignal
from PyQt5.QtGui import QPixmap
from PyQt5.QtSvg import QSvgWidget
from PyQt5.QtWidgets import QWidget, QGridLayout, QLabel, QHBoxLayout

from BasicCell import *


class PiecePicker(QWidget):
	pickerSignal = pyqtSignal()
	def __init__(self, parent, color):
		super().__init__(parent)
		# self.setGeometry(250, 50, 400, 100)
		self.move(250, 50)
		self.generalLayout = QHBoxLayout()
		self.setLayout(self.generalLayout)
		self.pickedPiece = None
		self.renderPieceItems(color)



	def renderPieceItem(self, piece):
		pickItemCell = CellPickItem(self, piece)
		pickItemCell.raise_()
		pickItemCell.pickSignal.connect(self.onPickItem)
		pickItemCell.setStyleSheet('''background-color: #ccc''')

		self.generalLayout.addWidget(pickItemCell)

	def renderPieceItems(self, color):
		piecePickArr = [
			chess.Piece(chess.QUEEN, color),
			chess.Piece(chess.ROOK, color),
			chess.Piece(chess.KNIGHT, color),
			chess.Piece(chess.BISHOP, color)
		]

		for piece in piecePickArr:
			self.renderPieceItem(piece)




	@pyqtSlot()
	def onPickItem(self):
		item = self.sender()
		self.pickedPiece = item
		self.pickerSignal.emit()

