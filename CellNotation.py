import sys

import chess
import chess.svg

from PyQt5 import QtGui
from PyQt5.QtCore import pyqtSlot, Qt, pyqtSignal
from PyQt5.QtGui import QPixmap, QFont
from PyQt5.QtSvg import QSvgWidget
from PyQt5.QtWidgets import QWidget, QGridLayout, QLabel
from Constants import Constants


class CellNotation(QLabel):
	def __init__(self, parent, textNotation):
		super().__init__(parent)

		self.setText(textNotation)
		self.squareSize = Constants.CELL_SIZE
		self.resize(self.squareSize, self.squareSize)
		self.setStyleSheet('''background-color: transparent; text-align: center''')


