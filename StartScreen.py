import sys

from PyQt5.QtCore import pyqtSlot, Qt, pyqtSignal
from PyQt5 import QtGui, QtCore
from PyQt5.QtGui import QFont, QPainter
from PyQt5.QtSvg import QSvgWidget, QSvgRenderer
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QGridLayout, QVBoxLayout, QLabel, QPushButton, QHBoxLayout
from Constants import Constants


class StartScreen(QWidget):
	def __init__(self, parent):
		super().__init__(parent)

		self.resize(Constants.MAIN_WINDOW_WIDTH, Constants.MAIN_WINDOW_HEIGHT)
		self.font = QFont()
		self.font.setFamily("PT Sans")
		self.font.setPixelSize(26)
		self.setFont(self.font)

		self.boxHLayout = QVBoxLayout()
		self.setLayout(self.boxHLayout)

		self.startGameButton = QPushButton('Start game', self)
		self.startGameButton.setStyleSheet('''padding: 10px''')



		self.boxHLayout.addWidget(self.startGameButton)






