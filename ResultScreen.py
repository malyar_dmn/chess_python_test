import sys

from PyQt5.QtCore import pyqtSlot, Qt, pyqtSignal
from PyQt5 import QtGui, QtCore
from PyQt5.QtGui import QFont, QPalette, QColor
from PyQt5.QtSvg import QSvgWidget
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QGridLayout, QVBoxLayout, QLabel, QPushButton, QHBoxLayout
from Constants import Constants


class ResultScreen(QWidget):
	clickSignal = pyqtSignal(str)
	def __init__(self, parent):
		super().__init__(parent)

		self.resize(Constants.MAIN_WINDOW_WIDTH, Constants.MAIN_WINDOW_HEIGHT)
		self.mainLayout = QVBoxLayout()
		myPalette = self.palette()
		resultScreenBgColor = QColor(0, 0, 0)
		resultScreenBgColor.setAlpha(200)
		myPalette.setColor(QPalette.Background, resultScreenBgColor)
		self.setAutoFillBackground(True)
		self.setPalette(myPalette)
		self.setLayout(self.mainLayout)

		modalWidg = self.modalResult()
		self.mainLayout.addWidget(modalWidg)
		self.mainLayout.setAlignment(Qt.AlignCenter)

	def clickHandler(self, params):
		sender = self.sender()

		self.clickSignal.emit('hehe')


	def modalResult(self):
		modalWidget = QWidget(self)
		modalLayout = QVBoxLayout()
		modalWidget.setLayout(modalLayout)

		modalLabel = QLabel('Game result', self)
		font = QFont()
		font.setFamily("PT Sans")
		font.setPixelSize(26)
		modalLabel.setFont(font)

		modalButtonsWidget = QWidget(modalWidget)
		modalButtonsWidget.show()
		modalButtonsLayout = QHBoxLayout()
		modalButtonsWidget.setLayout(modalButtonsLayout)

		buttonAccept = QPushButton('Start new game', self)
		buttonCancel = QPushButton('Exit', self)
		buttonAccept.clicked.connect(self.clickHandler)
		buttonCancel.clicked.connect(self.clickHandler)
		modalButtonsLayout.addWidget(buttonAccept)
		modalButtonsLayout.addWidget(buttonCancel)
		modalLayout.addWidget(modalLabel)
		modalLayout.addWidget(modalButtonsWidget)

		modalPalette = modalWidget.palette()
		modalBgColor = QColor(255, 255, 255)
		modalPalette.setColor(QPalette.Background, modalBgColor)
		modalWidget.setAutoFillBackground(True)
		modalWidget.setPalette(modalPalette)
		modalWidget.show()
		return modalWidget


