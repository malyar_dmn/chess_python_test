import sys

import chess
import chess.svg

from PyQt5.QtCore import pyqtSlot, Qt, pyqtSignal
from PyQt5 import QtGui, QtCore
from PyQt5.QtGui import QFont, QPixmap
from PyQt5.QtSvg import QSvgWidget
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QGridLayout, QVBoxLayout, QLabel, QPushButton, \
	QHBoxLayout, QGraphicsDropShadowEffect, QDialog
from Board import Board

from Cell import Cell
from Constants import Constants
from PiecePickerWidget import PiecePicker
from StartScreen import StartScreen
from ResultScreen import *

class MainWindow(QMainWindow):

	def __init__(self):
		super().__init__()
		self.setGeometry(300, 30, 1000, 1000)

		self.setWindowTitle('Chess')
		self.startGameWidget()
		self.generalLayout = QVBoxLayout()
		# self.generalLayout.addWidget(self.board)
		# self.generalLayout.addWidget(self.moveStack)
		self.widget = QWidget(self)
		self.setCentralWidget(self.widget)
		self.widget.setLayout(self.generalLayout)
		self.generalLayout.addWidget(self.startScreen)
		self.generalLayout.setAlignment(Qt.AlignCenter)

		# endModal = ModalFactory.getEndGameModal(self)
		# result = ResultScreen(self)
		# button = QPushButton('button', self)
		# button.setMaximumSize(100, 50)
		# result.mainLayout.addWidget(button)
		# result.mainLayout.setAlignment(Qt.AlignCenter)

	# self.generalLayout.setAlignment(Qt.AlignTop)
		#
		# self.board.actionSignal.connect(self.handleAction)
		# self.newGameButton.clicked.connect(self.handleNewGameButton)



	def startGameWidget(self):
		self.startScreen = StartScreen(self)
		self.startScreen.startGameButton.clicked.connect(self.startGame)

	def startGame(self):
		self.startScreen.close()
		self.board = Board(self)
		self.board.resultScreenSignal.connect(self.handleResultScreen)
		self.board.setMinimumSize(Constants.BOARD_WIDTH, Constants.BOARD_HEIGHT)
		self.newGameButton = QPushButton('Start new game', self)

		font = QFont()
		font.setFamily("PT Sans")
		font.setPixelSize(26)
		self.newGameButton.setFont(font)

		self.generalLayout.addWidget(self.newGameButton)
		self.generalLayout.addWidget(self.board)
		self.generalLayout.setAlignment(Qt.AlignCenter)

		# self.board.actionSignal.connect(self.handleAction)
		self.newGameButton.clicked.connect(self.handleNewGameButton)

	def resultGame(self):
		self.result = ResultScreen(self)
		self.result.clickSignal.connect(self.test)
		self.result.show()

	@pyqtSlot(str)
	def handleResultScreen(self, str):
		if str == 'game_over':
			self.resultGame()

	def test(self, param):
		print(param)
		self.handleNewGameButton()
		self.result.close()

	def handleNewGameButton(self):
		self.board.startNewGame()


	# @pyqtSlot('PyQt_PyObject')
	# def handleAction(self, message):
	# 	print('click')
	# 	if message:
	# 		print(message.message)
			# self.moveStack.setLabelText(str(message.message))

if __name__ == "__main__":
	MyChess = QApplication(sys.argv)
	window = MainWindow()
	window.show()
	sys.exit(MyChess.exec_())