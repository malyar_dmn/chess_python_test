import sys

import chess
import chess.svg

from PyQt5.QtCore import pyqtSlot, Qt, pyqtSignal
from PyQt5 import QtGui, QtCore
from PyQt5.QtGui import QPixmap, QPalette, QPainter
from PyQt5.QtSvg import QSvgWidget, QSvgRenderer
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QGridLayout, QVBoxLayout, QLabel
from Constants import Constants


class BasicCell(QLabel):
	mySignal = pyqtSignal()

	def __init__(self, parent):
		super().__init__(parent)
		self.cellWidth = Constants.CELL_SIZE
		self.cellHeight = Constants.CELL_SIZE
		self.color = None
		self.resize(self.cellWidth, self.cellHeight)

	def setBgColor(self, color):
		self.color = color
		self.setStyleSheet('''background-color: {}'''.format(self.color))




	def setPiece(self, pieceName):
		if pieceName is not None and pieceName.color is True:
			pixmap = QPixmap('assets/white_{}'.format(pieceName)).scaled(Constants.CELL_SIZE, Constants.CELL_SIZE)
			self.setPixmap(pixmap)

		elif pieceName is not None and pieceName.color is False:
			pixmap = QPixmap('assets/black_{}'.format(pieceName)).scaled(Constants.CELL_SIZE, Constants.CELL_SIZE)
			self.setPixmap(pixmap)

		else:
			pixmap = QPixmap()
			self.setPixmap(pixmap)


class BoardCell(BasicCell):
	def __init__(self, parent, squareIndex):
		super().__init__(parent)
		self.squareIndex = squareIndex
		self.squareCoordinates = chess.square_name(self.squareIndex)
		self.activeSquare = False



	def changeActiveSelection(self):
		if self.activeSquare:
			self.setStyleSheet('''border: 3px solid orange; background-color: {}'''.format(self.color))
		else:
			self.setStyleSheet('''border: none; background-color: {}'''.format(self.color))

	def mousePressEvent(self, event):
		self.mySignal.emit()


class CellPickItem(BasicCell):
	pickSignal = pyqtSignal()
	def __init__(self, parent, piece):
		super().__init__(parent)
		self.piece = piece
		self.setPiece(self.piece)


	def mousePressEvent(self, event):
		self.pickSignal.emit()

