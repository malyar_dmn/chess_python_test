import sys

import chess
import chess.svg

from PyQt5.QtCore import pyqtSlot, Qt, pyqtSignal
from PyQt5 import QtGui, QtCore
from PyQt5.QtGui import QFont, QPixmap
from PyQt5.QtSvg import QSvgWidget
from PyQt5.QtWidgets import QApplication, QWidget, QMainWindow, QGridLayout, QVBoxLayout, QLabel, QPushButton, \
	QHBoxLayout, QGraphicsDropShadowEffect

class ModalWidget(QWidget):
	def __init__(self, parent):
		super().__init__(parent)
		# self.initUI()



	def initUI(self):
		self.setGeometry(100, 300, 400, 400)
		shadow = QGraphicsDropShadowEffect(blurRadius=5, xOffset=3, yOffset=3)
		self.setGraphicsEffect(shadow)
		self.setStyleSheet('''background-color: white''')