import sys

import chess
import chess.svg

from PyQt5 import QtGui
from PyQt5.QtCore import pyqtSlot, Qt, pyqtSignal
from PyQt5.QtGui import QPixmap
from PyQt5.QtSvg import QSvgWidget
from PyQt5.QtWidgets import QWidget, QGridLayout, QLabel


class Cell(QLabel):
	mySignal = pyqtSignal()

	def __init__(self, parent, squareIndex):
		super().__init__(parent)
		self.squareSize = 80
		self.resize(self.squareSize, self.squareSize)
		self.squareIndex = squareIndex
		self.squareCoordinates = chess.square_name(self.squareIndex)
		self.setStyleSheet('''border: 1px solid black''')



	def setPiece(self, pieceName):
		if pieceName is not None and pieceName.color is True:
			pixmap = QPixmap('assets/white_{}'.format(pieceName)).scaled(self.squareSize, self.squareSize)
			self.setPixmap(pixmap)

		elif pieceName is not None and pieceName.color is False:
			pixmap = QPixmap('assets/black_{}'.format(pieceName)).scaled(self.squareSize, self.squareSize)
			self.setPixmap(pixmap)

		else:
			pixmap = QPixmap()
			self.setPixmap(pixmap)



	def highlightSquare(self):
		self.setStyleSheet('''background-color: #EF4C4C''')




	def mousePressEvent(self, event):
		self.mySignal.emit()